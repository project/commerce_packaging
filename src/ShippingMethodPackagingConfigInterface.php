<?php

namespace Drupal\commerce_packaging;

use Drupal\Core\Form\FormStateInterface;

interface ShippingMethodPackagingConfigInterface {

  /**
   * Gets whether the given inline form is supported.
   *
   * Confirms that:
   * - The inline form is used for plugin configuration.
   * - The plugin configuration is for commerce_shipping_method plugins.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   TRUE if the form is supported, FALSE otherwise.
   */
  public function supportsForm(array &$inline_form, FormStateInterface $form_state);

  /**
   * Alters the inline form.
   *
   * Adds the packaging configuration elements
   * to the form.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function alterForm(array &$inline_form, FormStateInterface $form_state);

}
