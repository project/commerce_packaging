<?php

namespace Drupal\commerce_packaging;


use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;

interface ShipmentPackageManagerInterface {

  /**
   * Gets the packaging strategy.
   *
   * If a shipping method is not provided, the
   * shipping method will be loaded from the shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment.
   * @param \Drupal\commerce_shipping\Entity\ShippingMethodInterface|null $shipping_method
   *   (Optional) The shipping method.
   *
   * @return \Drupal\commerce_packaging\Entity\PackagingStrategyInterface|null
   *   The packaging strategy, or NULL if not found.
   */
  public function getPackagingStrategy(ShipmentInterface $shipment, ShippingMethodInterface $shipping_method = NULL);

  /**
   * Gets the proposed shipment packages from the shipment.
   *
   * If a shipping method is not provided, the shipping method will
   * try to be loaded from the shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   * @param \Drupal\commerce_shipping\Entity\ShippingMethodInterface|null $shipping_method
   *   (Optional) The shipping method.
   *
   * @return \Drupal\commerce_packaging\ProposedShipmentPackage[]
   *   The proposed shipment packages.
   */
  public function getProposedShipmentPackages(ShipmentInterface $shipment, ShippingMethodInterface $shipping_method = NULL);

  public function setProposedShipmentPackages(ShipmentInterface $shipment, array $proposed_shipment_packages, ShippingMethodInterface $shipping_method = NULL);

  public function clearProposedShipmentPackages(ShipmentInterface $shipment);

  /**
   * Packages the shipment with proposed shipment packages.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   * @param \Drupal\commerce_shipping\Entity\ShippingMethodInterface|null $shipping_method
   *   (Optional) The shipping method.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment with proposed shipment packages.
   */
  public function packageShipment(ShipmentInterface $shipment, ShippingMethodInterface $shipping_method = NULL);

  /**
   * Finalizes the packages on the shipment.
   *
   * Converts proposed shipment packages to shipment package entities
   * and adds references to and from the shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment with packaged items.
   */
  public function finalizePackages(ShipmentInterface $shipment);

}
