<?php


namespace Drupal\commerce_packaging;


use Drupal\commerce\EntityHelper;
use Drupal\commerce\Plugin\Commerce\InlineForm\PluginConfiguration;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class ShippingMethodPackagingConfig implements ShippingMethodPackagingConfigInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ShippingMethodPackagingConfig constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsForm(array &$inline_form, FormStateInterface $form_state) {
    if (empty($inline_form['#inline_form']) || !($inline_form['#inline_form'] instanceof PluginConfiguration)) {
      return FALSE;
    }

    if ($inline_form['#inline_form']->getConfiguration()['plugin_type'] !== 'commerce_shipping_method') {
      return FALSE;
    }

    return TRUE;
  }

  public function alterForm(array &$inline_form, FormStateInterface $form_state) {
    $packaging_strategy_storage = $this->entityTypeManager->getStorage('commerce_packaging_strategy');
    $packaging_strategies = $packaging_strategy_storage->loadMultiple();

    /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\PluginConfiguration $inline_form_obj */
    $inline_form_obj = $inline_form['#inline_form'];
    $plugin_configuration = $inline_form_obj->getConfiguration()['plugin_configuration'];

    $inline_form['commerce_packaging'] = [
      '#type' => 'details',
      '#title' => $this->t('Packaging Options'),
      '#open' => TRUE,
    ];

    $inline_form['commerce_packaging']['packaging_strategy'] = [
      '#type' => 'select',
      '#title' => $this->t('Packaging Strategy'),
      '#options' => EntityHelper::extractLabels($packaging_strategies),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $plugin_configuration['commerce_packaging']['packaging_strategy'] ?? NULL,
    ];

    $inline_form['#element_validate'][] = [get_class($this), 'validateForm'];
    $inline_form['#commerce_element_submit'][] = [get_class($this), 'submitForm'];
  }

  /**
   * Validates the inline form.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function validateForm(array &$inline_form, FormStateInterface $form_state) {
    $values = $form_state->getValue($inline_form['#parents']);
    if (!empty($values['commerce_packaging'])) {
      // Preserve the packaging configuration in form state storage...
      // It is removed by the validation handler for shipping method plugins.
      $form_state->set('commerce_packaging', $values['commerce_packaging']);
    }
  }

  /**
   * Submits the inline form.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function submitForm(array &$inline_form, FormStateInterface $form_state) {
    $values = $form_state->getValue($inline_form['#parents']);
    if ($packaging_config = $form_state->get('commerce_packaging')) {
      $values['commerce_packaging'] = $packaging_config;
      $form_state->setValueForElement($inline_form, $values);
    }
  }

}
