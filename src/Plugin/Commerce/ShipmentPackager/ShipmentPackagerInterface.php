<?php

namespace Drupal\commerce_packaging\Plugin\Commerce\ShipmentPackager;

use Drupal\commerce_packaging\Entity\PackagingStrategyInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;

interface ShipmentPackagerInterface {

  /**
   * Create packages for the provided shipment.
   *
   * @param \Drupal\commerce_packaging\Entity\PackagingStrategyInterface $packaging_strategy
   *   The packaging strategy.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   * @param \Drupal\commerce_shipping\ShipmentItem[]
   *   The unpackaged shipment items.
   */
  public function packageItems(PackagingStrategyInterface $packaging_strategy, ShipmentInterface $shipment, array $unpackaged_items);

}
