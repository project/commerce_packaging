<?php


namespace Drupal\commerce_packaging;


use Drupal\commerce_packaging\Resolver\ChainPackagingStrategyResolverInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class ShipmentPackageManager implements ShipmentPackageManagerInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The chain packaging strategy resolver.
   *
   * @var \Drupal\commerce_packaging\Resolver\ChainPackagingStrategyResolverInterface
   */
  protected $packagingStrategyResolver;

  /**
   * ShipmentPackageManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\commerce_packaging\Resolver\ChainPackagingStrategyResolverInterface $packaging_strategy_resolver
   *   The chain packaging strategy resolver.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, ChainPackagingStrategyResolverInterface $packaging_strategy_resolver) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->packagingStrategyResolver = $packaging_strategy_resolver;
  }

  /**
   * {@inheritDoc}
   */
  public function getPackagingStrategy(ShipmentInterface $shipment, ShippingMethodInterface $shipping_method = NULL) {
    $packaging_strategy = NULL;
    if (!$shipping_method) {
      $shipping_method = $shipment->getShippingMethod();
    }
    if ($shipping_method) {
      $packaging_strategy = $this->packagingStrategyResolver->resolve($shipping_method, $shipment);
    }

    return $packaging_strategy;
  }

  /**
   * {@inheritDoc}
   */
  public function getProposedShipmentPackages(ShipmentInterface $shipment, ShippingMethodInterface $shipping_method = NULL) {
    $proposed_shipment_packages = [];

    $shipping_method_id = $shipping_method ? $shipping_method->id() : $shipment->getShippingMethodId();

    $proposed_shipment_packages_data = $shipment->getData('proposed_shipment_packages', []);
    if ($shipping_method_id && !empty($proposed_shipment_packages_data[$shipping_method_id])) {
      $proposed_shipment_packages = $proposed_shipment_packages_data[$shipping_method_id];
    }

    return $proposed_shipment_packages;
  }

  /**
   * {@inheritDoc}
   */
  public function setProposedShipmentPackages(ShipmentInterface $shipment, array $proposed_shipment_packages, ShippingMethodInterface $shipping_method = NULL) {
    if (!$shipping_method) {
      $shipping_method = $shipment->getShippingMethod();
    }

    if ($shipping_method) {
      $shipment_package_data = $shipment->getData('proposed_shipment_packages', []);
      $shipment_package_data[$shipping_method->id()] = $proposed_shipment_packages;
      $shipment->setData('proposed_shipment_packages', $shipment_package_data);
    }

    return $shipment;
  }

  /**
   * {@inheritDoc}
   */
  public function clearProposedShipmentPackages(ShipmentInterface $shipment) {
    $shipment->setData('proposed_shipment_packages', NULL);
    return $shipment;
  }

  /**
   * {@inheritDoc}
   */
  public function packageShipment(ShipmentInterface $shipment, ShippingMethodInterface $shipping_method = NULL) {
    if (!$shipping_method) {
      $shipping_method = $shipment->getShippingMethod();
    }

    if (!$shipping_method) {
      return $shipment;
    }

    $packaging_strategy = $this->getPackagingStrategy($shipment, $shipping_method);
    if ($packaging_strategy) {
      $shipment_packagers = $packaging_strategy->getShipmentPackagers();
      if (!empty($shipment_packagers)) {
        $shipment->setPackageType($packaging_strategy->getDefaultPackageType());

        $proposed_shipment_packages = [];
        $unpackaged_items = $shipment->getItems();

        foreach ($shipment_packagers as $shipment_packager) {
          list($proposed_shipment_packages, $unpackaged_items) = $shipment_packager->packageItems($packaging_strategy, $shipment, $unpackaged_items);
          if (empty($unpackaged_items)) {
            break;
          }
        }
        if (!empty($unpackaged_items)) {
          $this->messenger->addMessage($this->t('Not all of the shipment items were packaged!'));
        }

        $this->setProposedShipmentPackages($shipment, $proposed_shipment_packages, $shipping_method);
      }
    }

    return $shipment;
  }

  /**
   * {@inheritDoc}
   */
  public function finalizePackages(ShipmentInterface $shipment) {
    $proposed_shipment_packages = $this->getProposedShipmentPackages($shipment);

    $shipment_package_storage = $this->entityTypeManager->getStorage('commerce_shipment_package');
    $current_packages = $shipment->get('packages')->referencedEntities();
    $shipment_package_storage->delete($current_packages);

    $shipment_packages = [];
    /** @var \Drupal\commerce_packaging\ProposedShipmentPackage $proposed_shipment_package */
    foreach ($proposed_shipment_packages as $proposed_shipment_package) {
      /** @var \Drupal\commerce_packaging\Entity\ShipmentPackageInterface $shipment_package */
      $shipment_package = $shipment_package_storage->create([
        'type' => $proposed_shipment_package->getType(),
        'shipment_id' => [$shipment]
      ]);
      $shipment_package->populateFromProposedShipmentPackage($proposed_shipment_package);
      $shipment_packages[] = $shipment_package;
    }

    $shipment->set('packages', $shipment_packages);
    $this->clearProposedShipmentPackages($shipment);

    return $shipment;
  }

}
