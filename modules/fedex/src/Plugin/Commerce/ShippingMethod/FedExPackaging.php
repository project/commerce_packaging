<?php

namespace Drupal\commerce_packaging_fedex\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_fedex\FedExPluginManager;
use Drupal\commerce_fedex\FedExRequestInterface;
use Drupal\commerce_fedex\Plugin\Commerce\ShippingMethod\FedEx;
use Drupal\commerce_packaging\ShipmentPackageManagerInterface;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use NicholasCreativeMedia\FedExPHP\Enums\PhysicalPackagingType;
use NicholasCreativeMedia\FedExPHP\Structs\Money;
use NicholasCreativeMedia\FedExPHP\Structs\RequestedPackageLineItem;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FedExPackaging extends FedEx {

  /**
   * The shipment packager.
   *
   * @var \Drupal\commerce_packaging\ShipmentPackageManagerInterface
   */
  protected $shipmentPackager;

  /**
   * Constructs a new FedExPackaging object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_fedex\FedExPluginManager $fedex_service_manager
   *   The FedEx Plugin Manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The Event Dispatcher.
   * @param \Drupal\commerce_fedex\FedExRequestInterface $fedex_request
   *   The Fedex Request Service.
   * @param \Psr\Log\LoggerInterface $watchdog
   *   Commerce Fedex Logger Channel.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The price rounder.
   * @param \Drupal\commerce_packaging\ShipmentPackageManagerInterface $shipment_packager
   *   The shipment packager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, FedExPluginManager $fedex_service_manager, EventDispatcherInterface $event_dispatcher, FedExRequestInterface $fedex_request, LoggerInterface $watchdog, RounderInterface $rounder, ShipmentPackageManagerInterface $shipment_packager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager, $fedex_service_manager, $event_dispatcher, $fedex_request, $watchdog, $rounder);

    $this->shipmentPackager = $shipment_packager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('plugin.manager.commerce_fedex_service'),
      $container->get('event_dispatcher'),
      $container->get('commerce_fedex.fedex_request'),
      $container->get('logger.channel.commerce_fedex'),
      $container->get('commerce_price.rounder'),
      $container->get('commerce_packaging.shipment_package_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['options']['packaging'] = [
      '#type' => 'value',
      '#value' => $this->configuration['options']['packaging'],
      'notice' => ['#markup' => $this->t('<p><strong>NOTICE:</strong> Packaging strategy is being overridden by Commerce Packaging FedEx module.</p>')]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getRequestedPackageLineItems(ShipmentInterface $shipment) {
    return $this->getRequestedPackageLineItemsIndividual($shipment);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRequestedPackageLineItemsIndividual(ShipmentInterface $shipment) {
    $requested_package_line_items = [];
    $weightUnits = '';

    $proposed_shipment_packages = $this->shipmentPackager->getProposedShipmentPackages($shipment, $this->parentEntity);
    foreach ($proposed_shipment_packages as $delta => $proposed_shipment_package) {
      $requested_package_line_item = new RequestedPackageLineItem();
      if ($weightUnits == '') {
        /* All packages must have the same Weight Unit */
        $weightUnits = $proposed_shipment_package->getWeight()->getUnit();
      }

      $title = $proposed_shipment_package->getTitle();
      $title = preg_replace('/[^A-Za-z0-9 ]/', ' ', $title);
      $title = preg_replace('/ +/', ' ', $title);
      $title = trim($title);

      $requested_package_line_item
        ->setSequenceNumber($delta + 1)
        ->setGroupPackageCount(1)
        ->setWeight($this->physicalWeightToFedex($proposed_shipment_package->getWeight()->convert($weightUnits)))
        ->setDimensions($this->packageToFedexDimensions($proposed_shipment_package->getPackageType()))
        ->setPhysicalPackaging(PhysicalPackagingType::VALUE_BOX)
        ->setItemDescription($title);

      if ($this->configuration['options']['insurance']) {
        $requested_package_line_item->setInsuredValue(new Money(
          $proposed_shipment_package->getDeclaredValue()->getCurrencyCode(),
          $proposed_shipment_package->getDeclaredValue()->getNumber()
        ));
      }

      $this->adjustPackage($requested_package_line_item, $proposed_shipment_package->getItems(), $shipment);
      $requested_package_line_items[] = $requested_package_line_item;
    }

    return $requested_package_line_items;
  }

}
