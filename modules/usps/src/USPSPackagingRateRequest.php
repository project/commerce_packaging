<?php

namespace Drupal\commerce_packaging_usps;

use Drupal\commerce_packaging\ShipmentPackageManagerInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_usps\USPSRateRequest;
use Drupal\commerce_usps\USPSShipmentInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class USPSPackagingRateRequest extends USPSRateRequest {

  /**
   * The shipment packager.
   *
   * @var \Drupal\commerce_packaging\ShipmentPackageManagerInterface
   */
  protected $shipmentPackager;

  /**
   * USPSPackagingRateRequest constructor.
   *
   * @param \Drupal\commerce_usps\USPSShipmentInterface $usps_shipment
   *   The USPS shipment object.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(USPSShipmentInterface $usps_shipment, EventDispatcherInterface $event_dispatcher, ShipmentPackageManagerInterface $shipment_package_manager) {
    parent::__construct($usps_shipment, $event_dispatcher);
    $this->shipmentPackager = $shipment_package_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function getPackages() {
    $packages = [];

    if (!($this->uspsShipment instanceof USPSPackageShipmentInterface)) {
      return parent::getPackages();
    }

    $proposed_shipment_packages = $this->shipmentPackager->getProposedShipmentPackages($this->commerceShipment, $this->shippingMethod);
    foreach ($proposed_shipment_packages as $proposed_shipment_package) {
      $this->uspsShipment->setProposedShipmentPackage($proposed_shipment_package);
      $package = $this->uspsShipment->getPackage($this->commerceShipment);
      $packages[] = $package;
    }

    return $packages;
  }

  public function resolveRates(array $response) {
    $rates = [];

    // If there is only one package, the Package key will contain the Postage.
    $packages = [];
    if (!empty($response['RateV4Response']['Package']['Postage'])) {
      $packages[] = $response['RateV4Response']['Package'];
    }
    // If there are multiple packages, Package will be an array of items with Postage.
    else {
      $packages = $response['RateV4Response']['Package'];
    }

    foreach ($packages as $package) {
      // Parse the rate response and create shipping rates array.
      if (!empty($package['Postage'])) {

        // Convert the postage response to an array of rates when
        // only 1 rate is returned.
        if (!empty($package['Postage']['Rate'])) {
          $package['Postage'] = [$package['Postage']];
        }

        foreach ($package['Postage'] as $rate) {
          $price = $rate['Rate'];

          // Attempt to use an alternate rate class if selected.
          if (!empty($this->configuration['rate_options']['rate_class'])) {
            switch ($this->configuration['rate_options']['rate_class']) {
              case 'commercial_plus':
                $price = !empty($rate['CommercialPlusRate']) ? $rate['CommercialPlusRate'] : $price;
                break;
              case 'commercial':
                $price = !empty($rate['CommercialRate']) ? $rate['CommercialRate'] : $price;
                break;
            }
          }

          $service_code = $rate['@attributes']['CLASSID'];
          $service_name = $this->cleanServiceName($rate['MailService']);

          // Class code 0 is used for multiple services in the
          // response. The only way to determine which service
          // is returned is to parse the service name for matching
          // strings based on the service type. All other service
          // codes are unique and do not require this extra step.
          if ($service_code == 0) {
            if (stripos($service_name, 'Envelope') !== FALSE) {
              $service_code = self::FIRST_CLASS_MAIL_ENVELOPE;
            }
            elseif (stripos($service_name, 'Letter') !== FALSE) {
              $service_code = self::FIRST_CLASS_MAIL_LETTER;
            }
            elseif (stripos($service_name, 'Postcards') !== FALSE) {
              $service_code = self::FIRST_CLASS_MAIL_POSTCARDS;
            }
            elseif (stripos($service_name, 'Package') !== FALSE) {
              $service_code = self::FIRST_CLASS_MAIL_PACKAGE;
            }
            else {
              continue;
            }
          }

          // Only add the rate if this service is enabled.
          if (!in_array($service_code, $this->configuration['services'])) {
            continue;
          }

          if (empty($rates[$service_code])) {
            $rates[$service_code] = new ShippingRate([
              'shipping_method_id' => $this->shippingMethod->id(),
              'service' => new ShippingService($service_code, $service_name),
              'amount' => new Price($price, 'USD'),
            ]);
          }
          else {
            $rate = $rates[$service_code];
            $amount = $rate->getAmount();
            $amount = $amount->add(new Price($price, 'USD'));
            $rate->setAmount($amount);
          }
        }
      }
    }

    return $rates;
  }

}
