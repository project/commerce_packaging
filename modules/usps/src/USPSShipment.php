<?php

namespace Drupal\commerce_packaging_usps;

use Drupal\commerce_packaging\ProposedShipmentPackage;
use Drupal\commerce_usps\USPSShipment as USPSShipmentBase;

class USPSShipment extends USPSShipmentBase implements USPSPackageShipmentInterface {

  /**
   * The proposed shipment package.
   *
   * @var \Drupal\commerce_packaging\ProposedShipmentPackage
   */
  protected $proposedShipmentPackage;

  /**
   * {@inheritDoc}
   */
  public function setProposedShipmentPackage(ProposedShipmentPackage $proposed_shipment_package) {
    $this->proposedShipmentPackage = $proposed_shipment_package;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function setWeight() {
    if ($this->proposedShipmentPackage) {
      $weight = $this->proposedShipmentPackage->getWeight();

      if ($weight->getNumber() > 0) {
        $ounces = ceil($weight->convert('oz')->getNumber());

        $this->uspsPackage->setPounds(floor($ounces / 16));
        $this->uspsPackage->setOunces($ounces % 16);
      }
      return;
    }

    parent::setWeight();
  }

  /**
   * {@inheritDoc}
   */
  protected function getPackageType() {
    if ($this->proposedShipmentPackage) {
      return $this->proposedShipmentPackage->getPackageType();
    }

    return parent::getPackageType();
  }

}
