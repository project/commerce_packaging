<?php


namespace Drupal\commerce_packaging_usps;


use Drupal\commerce_packaging\ProposedShipmentPackage;

interface USPSPackageShipmentInterface {

  /**
   * Sets the proposed shipment package.
   *
   * @param \Drupal\commerce_packaging\ProposedShipmentPackage $proposed_shipment_package
   *   The proposed shipment package.
   *
   * @return $this
   */
  public function setProposedShipmentPackage(ProposedShipmentPackage $proposed_shipment_package);

}
