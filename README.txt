INSTALLATION
___________

The commerce_packaging module contains 3 sub-modules:
- FedEx
- UPS
- USPS

To enable rate calculations based on packaging provided by commerce_packaging, you must enable
the sub-module for the shipping method(s) you are using.

After enabling the appropriate sub-modules, you must configure your packaging strategies
and your shipping method configuration.

1. Go to /admin/commerce/config/packaging-strategy and add a packaging strategy.
   NOTE: The ALL IN ONE and INDIVIDUAL plugins will consume all shipment items
   and no other plugins will be run after them.
2. Go to /admin/commerce/shipping-methods and edit or create a shipping method.
3. In the PACKAGING OPTIONS details for the shipping method, select the packaging strategy.

To utilize manual packaging for product variations:

1. Go to /admin/commerce/config/product-variation-types and add or edit a product variation type.
2. Check the Manual Packaging Trait.
3. Edit or create a new Product Variation within a product.
4. Configure the manual packaging for the variation by setting a minimum and maximum quantity and
   choose the package type that will be used.  When the manual packager plugin is run, packages will
   be placed in the package type you choose if the order item quantity is equal to or within the minimum
   and maximum values.
